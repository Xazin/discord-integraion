**Discord GitLab Integration**

*How will this be achieved*

By using DSharpPlus and GitLab API to code a bot that will be able to push notifications of when and which member does a commit, merge or comment to a specific channel on the Discord Server. The settings of the Discord Bot itself will contain two configurable variables, which are Private Token and Project Path, needed for the API to 1. Find the gitlab project & 2. Have access to run Get Request for the needed information.

Translating time into MS or similar, each run will check if a new commit was made inbetween the last run and current run, if it was it will be pushed. The same integration will be used for merge and comment. 

The bot will compensate for lacking or simply replace "updating" the team of when a task is done, simply by pushing discord messages which will act as notifications. This will help leviate any Student Project or similar, who uses GitLab for their Version Control.

*Redundant Elements So Far*
Currently the Bot contains MySQL.Data.dll which was intended to be used to crossreference new commits, merges and comments. However, this might not be neccessary but if the integration of time-based-event checking does not manage to do the job properly, a database solution will be implemented instead as is more sufficient and guaranteed success, a database solution will also make the scaling of the bot a lot easier. 
