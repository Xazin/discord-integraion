﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordBot.Settings
{
    class Settings
    {
        public string PrivateToken { get; set; }
        public string ProjectPath { get; set; }
    }
}
