﻿using System;
using DSharpPlus;
using DiscordBot;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using System.Collections.Generic;
using DiscordBot.Project;

namespace DiscordBot
{
    class Program
    {
        static DiscordClient discord;

        static void Main(string[] args)
        {
            MainAsync(args).ConfigureAwait(false).GetAwaiter().GetResult();
        }

        static async Task MainAsync(string[] args)
        {
            Settings.Settings settings = new Settings.Settings();

            settings.ProjectPath = "5x1%2fmatematik-projekt";
            settings.PrivateToken = "SDz5ZtXUwtU2WzCJYoZE";

            RequestManager requestManager = new RequestManager();
            requestManager.settings = settings;
            Project.Project project = new Project.Project();

            // Get default project info
            dynamic projInfo = await requestManager.getProjectInfo();

            project.Name = projInfo["name"];
            project.Id = projInfo["id"];
            project.Creation = projInfo["created_at"];
            project.SshUrl = projInfo["ssh_url_to_repo"];
            project.HttpUrl = projInfo["http_url_to_repo"];
            project.WebUrl = projInfo["web_url"];
            project.IsEmpty = projInfo["empty_repo"];

            // Populate commits
            dynamic commits = await requestManager.getProjectCommits();
            project.commits = new List<Project.Commit>();

            // Console.WriteLine(commits);
            
            foreach (dynamic commit in commits)
            {
                Project.Commit tempCommit = new Project.Commit();

                tempCommit.Id = commit["id"];
                tempCommit.ShortId = commit["short_id"];
                tempCommit.Title = commit["title"];
                tempCommit.Message = commit["message"];
                tempCommit.Author = commit["author_name"];
                tempCommit.Email = commit["email"];
                tempCommit.Created = commit["created_at"];

                List<Project.Commit> tempCommits = project.commits;
                tempCommits.Add(tempCommit);

                project.commits = tempCommits;
            }

            Console.WriteLine("Project Commits: ");
            foreach (Commit commit in project.commits)
            {
                Console.WriteLine(commit.Author);
            }

            Console.ReadLine();

            /*
            discord = new DiscordClient(new DiscordConfiguration
            {
                Token = "NTU1NzM3OTI5NzE3MjUyMTAw.XIpRRw.VwWke4ahEbYA7TbrAJxKsLXo10c",
                TokenType = TokenType.Bot
            });

            discord.MessageCreated += async e =>
            {
                if (e.Message.Content.ToLower().StartsWith("!g."))
                {
                    string command = e.Message.Content.Split("!g.")[1];

                    string suffix = "";

                    if (command.Contains(" "))
                    {
                        suffix = command.Split(" ")[1];
                        command = command.Split(" ")[0];
                    }

                    switch (command)
                    {
                        case "ping":
                            await e.Message.RespondAsync("pong!");
                            break;
                        case "hello":
                            DiscordUser sender = e.Message.Author;
                            string author = sender.Mention;
                            await e.Message.RespondAsync("Hello to you aswell " + author);
                            break;
                        case "spam":
                            int amount = 11;

                            if (suffix != "")
                            {
                                int.TryParse(suffix, out amount);
                            }

                            for (int i = 0; i < amount; i++)
                            {
                                int where = i + 1;
                                await e.Message.RespondAsync("Spam engaged! [" + where + "/" + amount + "]");
                            }

                            break;
                        case "kick":
                            if (suffix == "") {
                                await e.Message.RespondAsync("No target of command, Usage: ```!g.kick @user```");
                            } else
                            {


                                ulong channelId = e.Message.ChannelId;
                                DiscordChannel channel = await discord.GetChannelAsync(channelId);
                                DiscordGuild guild = await discord.GetGuildAsync(channel.GuildId);

                                IReadOnlyList<DiscordMember> members = guild.Members;

                                foreach (var member in members)
                                {
                                    if (member.Mention == suffix)
                                    {
                                        // BAN: await guild.BanMemberAsync(member, 0, "Banned by GodLess Bot");
                                        await member.RemoveAsync("You have been kicked by GodLess Bot");
                                        await e.Message.RespondAsync(member.DisplayName + " has been kicked!");
                                    }
                                }
                            }
                            break;
                        default:
                            await e.Message.RespondAsync("Unknown Command!");
                            break;
                    }
                }
            };
            await discord.ConnectAsync();
            await Task.Delay(-1);
            */
        }
    }
}
