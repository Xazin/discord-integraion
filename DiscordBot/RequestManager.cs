﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace DiscordBot
{
    class RequestManager
    {
        private static HttpClient client = new HttpClient();
        public Settings.Settings settings { get; set; }

        public async void sendRequest()
        {
            string token = "SDz5ZtXUwtU2WzCJYoZE";
            
            // Get the default project information
            HttpResponseMessage response = await client.GetAsync("https://gitlab.com/api/v4/projects/5x1%2fmatematik-projekt/?private_token="+token);

            // Commits
            //HttpResponseMessage response = await client.GetAsync("https://gitlab.com/api/v4/projects/5x1%2fmatematik-projekt/repository/commits?private_token=" + token);

            string responseString = await response.Content.ReadAsStringAsync();

            Console.Write(responseString);
            Console.ReadLine();
        }

        // Get Default Project Information
        public async Task<dynamic> getProjectInfo()
        {
            HttpResponseMessage response = await client.GetAsync("https://gitlab.com/api/v4/projects/" + settings.ProjectPath + "?private_token=" + settings.PrivateToken);
            string responseString = await response.Content.ReadAsStringAsync();
            dynamic data = JsonConvert.DeserializeObject(responseString);

            return data;
        }

        public async Task<dynamic> getProjectCommits()
        {
            HttpResponseMessage response = await client.GetAsync("https://gitlab.com/api/v4/projects/" + settings.ProjectPath + "/repository/commits?private_token=" + settings.PrivateToken);
            string responseString = await response.Content.ReadAsStringAsync();
            dynamic data = JsonConvert.DeserializeObject(responseString);

            return data;
        }

    }
}
