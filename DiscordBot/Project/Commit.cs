﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordBot.Project
{
    class Commit
    {
        public string Id { get; set; } // data['id']
        public string ShortId { get; set; } // data['short_id']
        public string Title { get; set; } // data['title']
        public string Message { get; set; } // data['message']
        public string Author { get; set; } // data['author']
        public string Email { get; set; } // data['email']
        public string Created { get; set; } // data['created_at']
    }
}
