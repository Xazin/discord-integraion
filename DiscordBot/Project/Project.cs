﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DiscordBot.Project
{
    class Project
    {
        public string Id { get; set; } // data['id']
        public string Name { get; set; } // data['name']
        public string Creation { get; set; } // data['created_at']
        public string WebUrl { get; set; } // data['web_url']
        public string HttpUrl { get; set; } // data['http_url_to_repo']
        public string SshUrl { get; set; } // data['ssh_url_to_repo']
        public string IsEmpty { get; set; } // data['empty_repo']

        public string AmountOfCommits { get; set; } //

        public List<Commit> commits { get; set; } // Populated by RequestManager
    }
}
